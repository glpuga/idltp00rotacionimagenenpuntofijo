
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

/* Esto es por compatibilidad windows/linux (s�lo necesario porque en windows todo apesta) */
#ifndef O_BINARY
#define O_BINARY 0
#endif

typedef struct {

	char headerField[2];   /* Header identificatorio del archivo. */

	uint32_t fileSize;     /* Tama�� del archivo en bytes. */
	uint16_t reserved1;    /* Campo reservado, depende de la aplicaci�n generadora del archivo. */
	uint16_t reserved2;    /* Campo reservado, depende de la aplicaci�n generadora del archivo. */

	uint32_t bitmapOffset; /* Offset en bytes hasta el comienzo de la imagen. */

} __attribute__((packed)) bmpFileHeaderType;

typedef struct {

	uint32_t headerSize;    /* Tama�o en bytes de este header. */

	uint32_t bitmapWidth;   /* Ancho en pixels de la imagen. */
	uint32_t bitmapHeight;  /* Alto en pixels de la imagen. */
	uint16_t colorPlanes;   /* Cantidad de Color Planes. Siempre debe valer 1. */

	uint16_t bitsPerPixel;  /* Cantidad de bits por cada pixel. */

	uint32_t compressionMethod;  /* Tipo de compresi�n. 0 = no compression. */
	uint32_t rawImageSize;       /* Tama�o de la imagen.  */

	uint32_t xResolution; /* Resoluci�n horizontal de la imagen. */
	uint32_t yResolution; /* Resoluci�n horizontal de la imagen. */

	uint32_t paletteColors;   /* Cantidad de colores en la paleta. 0 significa 2^bits_per_pixel. */
	uint32_t importantColors; /* No se usa. */

} __attribute__((packed)) bmpBitmapInfoHeaderType;


typedef struct {

	uint8_t colorEntry[4]; /* Entrada en la paleta de colores. */

} __attribute__((packed)) bmpColorEntryType;

typedef struct {

	bmpFileHeaderType fileHeader;

	bmpBitmapInfoHeaderType infoHeader;

	bmpColorEntryType *colorPalette;

	uint8_t *rowPackedImage;

} bmpFileDataType;


int32_t cosTable[] = {
		1024, 1024, 1023, 1023, 1022, 1020, 1018, 1016,
		1014, 1011, 1008, 1005, 1002, 998, 994, 989,
		984, 979, 974, 968, 962, 956, 949, 943,
		935, 928, 920, 912, 904, 896, 887, 878,
		868, 859, 849, 839, 828, 818, 807, 796,
		784, 773, 761, 749, 737, 724, 711, 698,
		685, 672, 658, 644, 630, 616, 602, 587,
		573, 558, 543, 527, 512, 496, 481, 465,
		449, 433, 416, 400, 384, 367, 350, 333,
		316, 299, 282, 265, 248, 230, 213, 195,
		178, 160, 143, 125, 107, 89, 71, 54,
		36, 18, 0, -18, -36, -54, -71, -89,
		-107, -125, -143, -160, -178, -195, -213, -230,
		-248, -265, -282, -299, -316, -333, -350, -367,
		-384, -400, -416, -433, -449, -465, -481, -496,
		-512, -527, -543, -558, -573, -587, -602, -616,
		-630, -644, -658, -672, -685, -698, -711, -724,
		-737, -749, -761, -773, -784, -796, -807, -818,
		-828, -839, -849, -859, -868, -878, -887, -896,
		-904, -912, -920, -928, -935, -943, -949, -956,
		-962, -968, -974, -979, -984, -989, -994, -998,
		-1002, -1005, -1008, -1011, -1014, -1016, -1018, -1020,
		-1022, -1023, -1023, -1024, -1024, -1024, -1023, -1023,
		-1022, -1020, -1018, -1016, -1014, -1011, -1008, -1005,
		-1002, -998, -994, -989, -984, -979, -974, -968,
		-962, -956, -949, -943, -935, -928, -920, -912,
		-904, -896, -887, -878, -868, -859, -849, -839,
		-828, -818, -807, -796, -784, -773, -761, -749,
		-737, -724, -711, -698, -685, -672, -658, -644,
		-630, -616, -602, -587, -573, -558, -543, -527,
		-512, -496, -481, -465, -449, -433, -416, -400,
		-384, -367, -350, -333, -316, -299, -282, -265,
		-248, -230, -213, -195, -178, -160, -143, -125,
		-107, -89, -71, -54, -36, -18, 0, 18,
		36, 54, 71, 89, 107, 125, 143, 160,
		178, 195, 213, 230, 248, 265, 282, 299,
		316, 333, 350, 367, 384, 400, 416, 433,
		449, 465, 481, 496, 512, 527, 543, 558,
		573, 587, 602, 616, 630, 644, 658, 672,
		685, 698, 711, 724, 737, 749, 761, 773,
		784, 796, 807, 818, 828, 839, 849, 859,
		868, 878, 887, 896, 904, 912, 920, 928,
		935, 943, 949, 956, 962, 968, 974, 979,
		984, 989, 994, 998, 1002, 1005, 1008, 1011,
		1014, 1016, 1018, 1020, 1022, 1023, 1023, 1024};


int32_t cosQ10(uint32_t angle)
{
	angle = angle % 360;

	return cosTable[angle];
}

int32_t sinQ10(uint32_t angle)
{
	return cosQ10(angle + 270);
}

void bmpRotateCanvas(uint8_t *srcCanvas, uint8_t *dstCanvas, int32_t imageWidth, int32_t imageHeight, uint32_t angle)
{
	int32_t dstX, dstY;
	int32_t srcX, srcY;
	int32_t xRel, yRel;

	for (dstY = 0; dstY < imageHeight; dstY++)
	{
		for (dstX = 0; dstX < imageWidth; dstX++)
		{
			xRel = dstX - imageWidth / 2;
			yRel = dstY - imageHeight / 2;

			/* Para cada pixel nuevo calculo sus coordenadas anteriores */
			srcX = (xRel * cosQ10(angle) + yRel * sinQ10(angle)) / 1024 + imageWidth / 2;
			srcY = (yRel * cosQ10(angle) - xRel * sinQ10(angle)) / 1024 + imageHeight / 2 ;

			if (((srcX >= 0) && (srcX < imageWidth)) && ((srcY >= 0) && (srcY < imageHeight)))
			{
				dstCanvas[dstY*imageWidth + dstX] = srcCanvas[srcY*imageWidth + srcX];
			} else {

				dstCanvas[dstY*imageWidth + dstX] = 0;
			}
		}
	}
}

void bmpDisplayFileHeader(bmpFileHeaderType *fileHeader)
{

	printf("BMP File Header\r\n");
	printf(" - %20s : %c%c\r\n", "HeaderField", fileHeader->headerField[0], fileHeader->headerField[1]);
	printf(" - %20s : %u\r\n", "FileSize",     (unsigned int) fileHeader->fileSize);
	printf(" - %20s : %u\r\n", "BitmapOffset", (unsigned int) fileHeader->bitmapOffset);
	printf("\r\n");

}

void bmpDisplayInfoHeader(bmpBitmapInfoHeaderType *infoHeader)
{
	printf("BMP Info Header\r\n");
	printf(" - %20s : %u (%8.8x)\r\n", "HeaderSize",        (unsigned int)infoHeader->headerSize, (unsigned int)infoHeader->headerSize);
	printf(" - %20s : %u\r\n", "BitmapWidth",       (unsigned int)infoHeader->bitmapWidth);
	printf(" - %20s : %u\r\n", "BitmapHeight",      (unsigned int)infoHeader->bitmapHeight);
	printf(" - %20s : %u\r\n", "ColorPlanes",       (unsigned int)infoHeader->colorPlanes);
	printf(" - %20s : %u\r\n", "bitsPerPixel",      (unsigned int)infoHeader->bitsPerPixel);
	printf(" - %20s : %u\r\n", "CompressionMethod", (unsigned int)infoHeader->compressionMethod);
	printf(" - %20s : %u\r\n", "RawImageSize",      (unsigned int)infoHeader->rawImageSize);
	printf(" - %20s : %u\r\n", "xResolution",       (unsigned int)infoHeader->xResolution);
	printf(" - %20s : %u\r\n", "yResolution",       (unsigned int)infoHeader->yResolution);
	printf(" - %20s : %u\r\n", "PaletteColors",     (unsigned int)infoHeader->paletteColors);
	printf(" - %20s : %u\r\n", "ImportantColors",   (unsigned int)infoHeader->importantColors);
	printf("\r\n");

}

bmpFileDataType *bmpLoadFile(const char *filename)
{
	bmpFileDataType *fileData;
	const char *errorStr;
	int32_t retVal, badFile, paletteSize;
	int32_t fh;

	fh = open(filename, O_RDONLY);

	if (fh < 0)
	{
		fprintf(stderr, "No se pudo abrir el archivo %s.\r\n", filename);
		exit(1);
	}

	fileData = malloc(sizeof(bmpFileDataType));
	if (fileData == NULL)
	{
		fprintf(stderr, "No hay suficiente memoria!\r\n");
		exit(1);
	}

	retVal = read(fh, (void*)&fileData->fileHeader, sizeof(bmpFileHeaderType));
	if (retVal < 0)
	{
		fprintf(stderr, "Error al leer la cabecera del archivo!\r\n");
		free(fileData);
		exit(1);
	}

	retVal = read(fh, (void*)&fileData->infoHeader, sizeof(bmpBitmapInfoHeaderType));
	if (retVal < 0)
	{
		fprintf(stderr, "Error al leer la cabecera de informaci�n adicional!\r\n");
		free(fileData);
		exit(1);
	}

	/* Muestro el contenido de los headers (para debug) . */
	// bmpDisplayFileHeader(&fileData->fileHeader);
	// bmpDisplayInfoHeader(&fileData->infoHeader);

	/* Seg�n el estandar, paletteColors puede valer cero para significar el valor por defecto, 2^bits */
	if (fileData->infoHeader.paletteColors == 0)
	{
		paletteSize = sizeof(bmpColorEntryType) * (int)(1 << fileData->infoHeader.bitsPerPixel);
	} else {
		paletteSize = sizeof(bmpColorEntryType) * fileData->infoHeader.paletteColors;
	}

	/* Valido que sea una imagen que puedo procesar. */
	badFile = 0;
	if ((fileData->fileHeader.headerField[0] != 'B') || (fileData->fileHeader.headerField[1] != 'M'))
	{
		errorStr = "String de identificaci�n inv�lido";
		badFile  = 1;
	}

	if (fileData->infoHeader.colorPlanes != 1)
	{
		errorStr = "Color Planes != 1";
		badFile  = 1;
	}

	if (fileData->infoHeader.bitsPerPixel != 8)
	{
		errorStr = "S�lo puede procesarse archivos de 8 bits por color (paleta indexada)";
		badFile  = 1;
	}

	if (fileData->infoHeader.compressionMethod != 0)
	{
		errorStr = "S�lo puede procesarse archivos sin compresi�n.";
		badFile  = 1;
	}

	if (fileData->fileHeader.bitmapOffset != (sizeof(fileData->fileHeader) + fileData->infoHeader.headerSize + paletteSize))
	{
		errorStr = "El archivo contiene campos adicionales que no puedo procesar!.";
		badFile  = 1;
	}

	if (sizeof(fileData->infoHeader) > fileData->infoHeader.headerSize)
	{
		errorStr = "El info header corresponde a una versi�n antigua del formato BMP!";
		badFile  = 1;
	}

	if (badFile != 0)
	{
		fprintf(stderr, "Tipo de archivo inv�lido. Error: %s\r\n", errorStr);
		free(fileData);
		exit(1);
	}

	/* Si el tama�o del info header era mayor que el que yo le� del archivo, avanzo el cursor del archivo la cantidad necesaria y
	 * corrijo el campo correspondiente en el header. */
	if (sizeof(fileData->infoHeader) != fileData->infoHeader.headerSize)
	{
		lseek(fh, fileData->infoHeader.headerSize - sizeof(fileData->infoHeader), SEEK_CUR);

		fileData->infoHeader.headerSize   = sizeof(bmpBitmapInfoHeaderType);
		fileData->fileHeader.bitmapOffset = sizeof(fileData->fileHeader) + sizeof(bmpBitmapInfoHeaderType) + paletteSize;
	}

	/* Levanto la informaci�n de la tabla de colores */

	/* Reservo la cantidad de memoria necesaria */
	fileData->colorPalette = malloc(paletteSize);
	if (fileData->colorPalette == NULL)
	{
		fprintf(stderr, "No hay suficiente memoria!\r\n");
		free(fileData);
		exit(1);
	}

	/* Y leo la tabla */
	retVal = read(fh, (void*)fileData->colorPalette, paletteSize);
	if (retVal < 0)
	{
		fprintf(stderr, "Error al leer la tabla de colores!\r\n");
		free(fileData->colorPalette);
		free(fileData);
		exit(1);
	}

	/* Finalmente leo la imagen. */

	/* Reservo la cantidad de memoria necesaria */
	fileData->rowPackedImage = malloc(fileData->infoHeader.rawImageSize);
	if (fileData->rowPackedImage == NULL)
	{
		fprintf(stderr, "No hay suficiente memoria!\r\n");
		free(fileData->colorPalette);
		free(fileData);
		exit(1);
	}

	retVal = read(fh, (void*)fileData->rowPackedImage, fileData->infoHeader.rawImageSize);
	if (retVal < 0)
	{
		fprintf(stderr, "Error al leer la tabla de colores!\r\n");
		free(fileData->rowPackedImage);
		free(fileData->colorPalette);
		free(fileData);
		exit(1);
	}

	close(fh);

	return fileData;
}


bmpFileDataType *bmpSaveFile(bmpFileDataType *fileData, const char *filename)
{
	int32_t retVal1, retVal2, retVal3, retVal4;
	int32_t paletteSize;
	int32_t fh;

	fh = open(filename, O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, S_IRWXU);

	if (fh < 0)
	{
		fprintf(stderr, "No se pudo abrir el archivo de salida %s.\r\n", filename);
		exit(1);
	}

	/* Seg�n el estandar, paletteColors puede valer cero para significar el valor por defecto, 2^bits */
	if (fileData->infoHeader.paletteColors == 0)
	{
		paletteSize = sizeof(bmpColorEntryType) * (int)(1 << fileData->infoHeader.bitsPerPixel);
	} else {
		paletteSize = sizeof(bmpColorEntryType) * fileData->infoHeader.paletteColors;
	}

	retVal1 = write(fh, (void*)&fileData->fileHeader, sizeof(bmpFileHeaderType));
	retVal2 = write(fh, (void*)&fileData->infoHeader, sizeof(bmpBitmapInfoHeaderType));
	retVal3 = write(fh, (void*)fileData->colorPalette, paletteSize);
	retVal4 = write(fh, (void*)fileData->rowPackedImage, fileData->infoHeader.rawImageSize);

	if ((retVal1 < 0) || (retVal2 < 0) || (retVal3 < 0) || (retVal4 < 0))
	{
		fprintf(stderr, "Error al escribir el archivo en disco!\r\n");
		free(fileData->rowPackedImage);
		free(fileData->colorPalette);
		free(fileData);
		exit(1);
	}

	close(fh);

	return 0;
}


void bmpFreeFileData(bmpFileDataType *fileData)
{
	free(fileData->rowPackedImage);
	free(fileData->colorPalette);
	free(fileData);
}

void bmpBitmapToCanvas(uint8_t *rowPackedImage, uint8_t *canvas, int32_t imageWidth, int32_t imageHeight)
{
	int32_t x, y, roundedTo32Width;

	/* Calculo el tama�o de una linea redondeada al pr�ximo m�ltiplo de 32 bits */
	roundedTo32Width = ((imageWidth + 3) / 4) * 4;

	for (y = 0; y < imageHeight; y++)
	{
		for (x = 0; x < imageWidth; x++)
		{
			canvas[y*imageWidth + x] = rowPackedImage[(imageHeight - y - 1)*roundedTo32Width + x];
		}
	}
}

void bmpCanvasToBitmap(uint8_t *canvas, uint8_t *rowPackedImage, int32_t imageWidth, int32_t imageHeight)
{
	int32_t x, y, roundedTo32Width;

	/* Calculo el tama�o de una linea redondeada al pr�ximo m�ltiplo de 32 bits */
	roundedTo32Width = ((imageWidth + 3) / 4) * 4;

	for (y = 0; y < imageHeight; y++)
	{
		for (x = 0; x < imageWidth; x++)
		{
			rowPackedImage[(imageHeight - y - 1)*roundedTo32Width + x] = canvas[y*imageWidth + x];
		}
	}
}

void bmpProcessBitmap(bmpFileDataType *fileData, uint32_t angle)
{
	int32_t imageWidth, imageHeight;
	void *packedImage, *srcCanvas, *dstCanvas;

	packedImage = (void *)fileData->rowPackedImage;
	imageWidth  = fileData->infoHeader.bitmapWidth;
	imageHeight = fileData->infoHeader.bitmapHeight;

	/* Reservo espacio en memoria para la imagen en formato estandar (no row packed, como viene en el archivo) */

	srcCanvas = malloc(imageHeight * imageWidth);
	dstCanvas = malloc(imageHeight * imageWidth);

	if ((srcCanvas == NULL) || (dstCanvas == NULL))
	{
		fprintf(stderr, "No hay espacio en memoria!\r\n");
		bmpFreeFileData(fileData);
		exit(1);
	}
	bmpBitmapToCanvas(packedImage, srcCanvas, imageWidth, imageHeight);

	bmpRotateCanvas(srcCanvas, dstCanvas, imageWidth, imageHeight, angle);

	bmpCanvasToBitmap(dstCanvas, packedImage, imageWidth, imageHeight);

	free(srcCanvas);
	free(dstCanvas);
}

int main(int argc, const char *argv[]) {

	bmpFileDataType *fileData;
	int32_t angle;

	const char *inputFileName = "inputfile.bmp";
	const char *outputFileName = "outputfile.bmp";

	fprintf(stderr, "\r\n");
	fprintf(stderr, "IDL - Ejercicio entregable TP n� 00\r\n");
		fprintf(stderr, "\r\n");

	if (argc < 2)
	{
		fprintf(stderr, "Falta especificar el �ngulo a rotar la imagen!\r\n");
		fprintf(stderr, "\r\n");
		fprintf(stderr, "  # %s <angulo de 0 a 359>\r\n", argv[0]);
		fprintf(stderr, "\r\n");
		exit(1);
	}

	angle = atol(argv[1]);
	if ((angle < 0) || (angle >= 360))
	{
		fprintf(stderr, "El �ngulo especificado (%d) es inv�lido!\r\n", angle);
		fprintf(stderr, "\r\n");
		fprintf(stderr, "  # %s <angulo de 0 a 359>\r\n", argv[0]);
		fprintf(stderr, "\r\n");
		exit(1);
	}


	fprintf(stderr, "Cargando fichero BMP %s...\r\n", inputFileName);
	fileData = bmpLoadFile(inputFileName);

	fprintf(stderr, "Procesando imagen...\r\n");
	bmpProcessBitmap(fileData, angle);

	fprintf(stderr, "Guardando resultado en archivo %s...\r\n", outputFileName);
	bmpSaveFile(fileData, outputFileName);

	fprintf(stderr, "Liberando memoria...\r\n");
	bmpFreeFileData(fileData);

	fprintf(stderr, "Fin de programa.\r\n");
	fprintf(stderr, "\r\n");


	return EXIT_SUCCESS;
}
