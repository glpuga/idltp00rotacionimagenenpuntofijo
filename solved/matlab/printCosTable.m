vAngle = [0:359];

vCos = round(1024 * cos(pi / 180 * vAngle));


fprintf('int32_t cosTable[] = {');

for I = 1:length(vCos)
    
    
    if (I > 1);
        fprintf(', ');
    end
    
    if (floor((I-1)/8) * 8) == I-1
        fprintf('\n        ');
    end
       
    fprintf('%d', vCos(I))
end

fprintf('};\n');